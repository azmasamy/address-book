/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package addressbook;

import java.util.Scanner;
import java.util.ArrayList;

/**
 *
 * @author HamzaTaher
 */
public class AddressBook {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int choice = 1;
        String firstName;
        String lastName;
        String address;
        String city;
        String state;
        int zip;
        String phoneNumber;
        Scanner n = new Scanner(System.in);
        ArrayList<Intry> listOfIntries = new ArrayList<>();

        //Intry i1 = new Intry("Hamza", "Taher", "Address", "Trukey", "Istanbul", 13111, "+09598765224");
        //System.out.println(i1.getEntryInfo());
        while (choice != 0) {

            switch (choice) {
                case 1: {
                    System.out.print("Enter First Name: ");
                    firstName = n.nextLine();
                    

                    System.out.print("Enter Last Name: ");
                    lastName = n.nextLine();

                    System.out.print("Enter Address: ");
                    address = n.nextLine();

                    System.out.print("Enter City: ");
                    city = n.nextLine();

                    System.out.print("Enter State: ");
                    state = n.nextLine();

                    System.out.print("Enter ZIP Code: ");
                    zip = n.nextInt();

                    System.out.print("Enter Phone Number: ");
                    phoneNumber = n.nextLine();
                    phoneNumber = n.nextLine();

                    listOfIntries.add(new Intry(firstName, lastName, address, city, state, zip, phoneNumber));
                    break;
                }
                case 2: {
                    int chosenIndex;
                    System.out.println("Enter The Index Of The Contact That You Want To Edit");
                    chosenIndex = n.nextInt();
                    if (chosenIndex > listOfIntries.size() || chosenIndex <= 0) {
                        System.err.println("Wrong Input, You don't have a contact at index " + chosenIndex);
                    } else {
                        System.out.println("The Contact You Chose Is");
                        System.out.println("Contact " + (chosenIndex) + " " + listOfIntries.get(chosenIndex - 1).getEntryInfo());

                        System.out.print("Enter The New Address: ");
                        address = n.nextLine();
                        address = n.nextLine();

                        System.out.print("Enter The New City: ");
                        city = n.nextLine();

                        System.out.print("Enter The New State: ");
                        state = n.nextLine();

                        System.out.print("Enter The New ZIP Code: ");
                        zip = n.nextInt();

                        System.out.print("Enter The New Phone Number: ");
                        phoneNumber = n.nextLine();
                        phoneNumber = n.nextLine();

                        listOfIntries.get(chosenIndex - 1).editEntryInfo(address, city, state, zip, phoneNumber);

                        System.out.println("Done, The Contact has Been Updated");
                        System.out.println("Contact " + (chosenIndex) + " " + listOfIntries.get(chosenIndex - 1).getEntryInfo());
                    }
                    break;
                }
                case 3: {
                    int chosenIndex;
                    int isSure;
                    System.out.println("Enter The Index Of The Contact That You Want To Remove");
                    chosenIndex = n.nextInt();
                    
                    if (chosenIndex > listOfIntries.size() || chosenIndex <= 0) {
                        System.err.println("Wrong Input, You don't have a contact at index " + chosenIndex);
                    } else {
                        
                        System.out.println("The Contact You Chose Is");
                        System.out.println("Contact " + (chosenIndex) + " " + listOfIntries.get(chosenIndex - 1).getEntryInfo());
                        
                        System.out.println("Are You Sure You That You Want To Remove This Contact Permenantly?");
                        System.out.println("Enter 1 To Confirm");
                        System.out.println("Enter Any Other Number To Cancel");
                        isSure = n.nextInt();
                        if(isSure == 1){
                            listOfIntries.remove(chosenIndex - 1);
                            System.out.println("Done, The Contact Has Been Removed");
                        } else {
                            System.out.println("The Operation Has Been Canceled");
                        }
                    }
                    
                    break;
                }
                case 4: {
                    System.out.println("You have " + listOfIntries.size() + " Contacts");
                    int index = 1;
                    for (Intry i : listOfIntries) {
                        System.out.println("Contact " + index++ + " " + i.getEntryInfo());
                    }
                    break;
                }
                
                default:
                    System.err.println("Invalid Input");
            }
            System.out.println("+ Options ++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            System.out.println("+ Enter 0 to save your Address Book and exit");
            System.out.println("+ Enter 1 to add a contact");
            System.out.println("+ Enter 2 to edit a contact");
            System.out.println("+ Enter 3 to remove a contact");
            System.out.println("+ Enter 4 to print all the contacts");
            System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

            choice = n.nextInt();
        }
    }
    
    public static void saveInties(ArrayList<Intry> ListOfInties){
        
    }

}
