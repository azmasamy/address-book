/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package addressbook;

/**
 *
 * @author HamzaTaher
 */
public class Intry {

    private String firstName;
    private String lastName;
    private String address;
    private String city;
    private String state;
    private int zip;
    private String phoneNumber;
    
    public Intry(String firstName, String lastName, String address, String city, String state, int zip, String phoneNumber){
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.phoneNumber = phoneNumber;
    }

    public void editEntryInfo(String address, String city, String state, int zip, String phoneNumber) {
        this.address = address;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.phoneNumber = phoneNumber;
    }
    
    public String getEntryInfo(){
        return "------------------------------------------------------\n" +
                "\tName : \t\t" + firstName + " " + lastName + "\n" +
                "\tAddress : \t" + address + " " + state + " " + city + " " + zip + "\n" +
                "\tPhone Number : \t" + phoneNumber + "\n" + 
                "----------------------------------------------------------------";
    }
    
}
